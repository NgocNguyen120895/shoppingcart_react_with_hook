import logo from './logo.svg';
import './App.css';
import ReactHookShoesShoppingEx from './Hook_ShoesShopping/ReactHookShoesShoppingEx';

function App() {
  return (
    <div>
      <ReactHookShoesShoppingEx/>
    </div>
  );
}

export default App;
