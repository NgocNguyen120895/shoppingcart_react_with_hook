import React from 'react'

export default function Product({ product, addToCart }) {
    return (
        <div>
            <div className="card border-primary">
                <img className="card-img-top" src={product.image} alt={product.id} />
                <div className="card-body bg-dark text-white">
                    <h4 className="card-title">{product.name}</h4>
                    <p className="card-text">{product.price}</p>
                    <button className='btn btn-primary' onClick={() => {
                        addToCart(product)
                    }}>Add</button>
                </div>

            </div >
        </div >

    )
}
