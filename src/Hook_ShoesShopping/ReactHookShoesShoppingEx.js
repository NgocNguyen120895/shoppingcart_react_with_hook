import React, { useState } from 'react'
import ProductList from './ProductList'
import ReactHookShoesCart from './ReactHookShoesCart'


export default function ReactHookShoesShoppingEx() {
    let [cart, setCart] = useState([])

    let addToCart = (addProduct) => {
        let clonedCart = [...cart]
        let index = clonedCart.findIndex(item => {
            return item.id === addProduct.id
        })
        if (index === -1) {
            let newAddProduct = { ...addProduct, qty: 1 };
            clonedCart.push(newAddProduct)
        } else {
            clonedCart[index].qty += 1
        }
        setCart(cart = clonedCart)
    }

    let removeItem = (index) => {
        let clonedCart = [...cart];
        clonedCart.splice(index, 1)
        setCart(cart = clonedCart)
    }

    let quantityChangeHandle = (id, value) => {

        let clonedCart = [...cart];
        let index = clonedCart.findIndex(item => {
            return item.id === id
        })

        if (value) {
            clonedCart[index].qty += 1
        } else
            if (clonedCart[index].qty > 1) {
                clonedCart[index].qty -= 1
            }
        setCart(cart = clonedCart)
    }


    return (
        <div className='container text-center'>
            <h2>Shoes Shopping Cart By Hook</h2>

            <ReactHookShoesCart cart={cart} removeItem={removeItem} quantityHandle={quantityChangeHandle} />
            <ProductList addToCart={addToCart} />
        </div>
    )
}
