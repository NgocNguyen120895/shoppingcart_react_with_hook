import React from 'react'
import Product from './Product'
import Shoes_Data from './ProductsData'



export default function ProductList({addToCart}) {
    return (
        <div className='row'>
            {renderProduct(addToCart)}
        </div>
    )
}

let renderProduct = (props) => {
    return Shoes_Data.map((product, index) => {
        return (
            <div className='col-3'>
                <Product product={product} addToCart={props} key={index} />
            </div>
        )
    })
}