import React from 'react'

export default function ReactHookShoesCart(props) {

    let { cart, removeItem, quantityHandle } = props;

    let renderCart = (cart) => {
        let mapCart = cart.map((item, index) => {
            return (
                <tr key={index} >
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.price}</td>
                    <td>
                        <button className='btn btn-primary' onClick={() => { quantityHandle(item.id, false) }}>-</button>
                        <span className='px-3'>
                            {item.qty}
                        </span>
                        <button className='btn btn-dark text-white' onClick={() => { quantityHandle(item.id, true) }}>+</button>
                    </td>
                    <td>
                        <img src={item.image} alt={item.image} width={100} height={100} />
                    </td>
                    <td>
                        <button className='btn btn-warning' onClick={() => { removeItem(index) }}>Remove</button>
                    </td>
                </tr >
            )
        })
        return mapCart;
    }

    // let tongSoTien = (arr) => {
    //     return arr.map((item) => {
    //         return (
    //             item.qty * item.price
    //         )
    //     })
    // }

    return (
        <div>
            <table className='table'>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>NAME</th>
                        <th>PRICE</th>
                        <th>QUANTITY</th>
                        <th>IMG</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    {renderCart(cart)}
                </tbody>
                <tfoot>
                    <tr>
                        <td colSpan="5">TOTAL: </td>
                        <td>
                            {/* {tongSoTien(cart)} */}
                            {cart.reduce((tongSo, cart, index) => {
                                return tongSo += cart.price * cart.qty
                            }, 0)}
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div >
    )
}

